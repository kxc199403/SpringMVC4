<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>服务器推送事件</title>
    <script src="../assets/js/jquery-3.2.1.min.js"></script>
</head>
<body>
<div id="msgFromPush"></div>
<script>
    if(!!window.EventSource){
        var source = new EventSource('push');
        s='';
        source.addEventListener('message',function (e) {
            s+=e.data+"<br>"
            $("#msgFromPush").html(s);
        });
        source.addEventListener('open',function (e) {
            console.log("连接打开");
        },false);
        source.addEventListener('erroe',function (e) {
            if(e.readyState == EventSource.CLOSED){
                console.log("连接关闭");
            }else {
                console.log(e.readyState);
            }
        },false);
    }else {
        console.log("你的浏览器不支持SSE");
    }
</script>
</body>
</html>
