<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>自定义数据格式</title>
    <script src="../assets/js/jquery-3.2.1.min.js"></script>
</head>
<body>
<div id="resp"></div>
<input type="button" onclick="req();" value="请求">
<script>
    function req() {
       $.ajax({
            url:"convert",
            data:"001-JAVA",
            type:"post",
            contentType:"application/x-kxc",
            success:function (data) {
                $("#resp").html(data);
            }
        });
    }
</script>
</body>
</html>
