package com.kxc.web;

import com.kxc.domain.DemoObj;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by kk on 2017/8/7.
 */
@Controller
public class ConverterController {
    @RequestMapping(value = "/convert",produces = {"application/x-kxc"})
    public @ResponseBody DemoObj convert(@RequestBody DemoObj demoObj){
        System.out.println("run ConverterController");
        return demoObj;
    }
}
