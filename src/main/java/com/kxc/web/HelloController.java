package com.kxc.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by kk on 2017/8/7.
 */
@Controller
public class HelloController {

    @RequestMapping("hello")
    public String hello(){
        System.out.println("run HelloController");
        return "hello";
    }
}
