package com.kxc.web;

import com.kxc.domain.DemoObj;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by kk on 2017/8/7.
 */
@RestController
@RequestMapping("rest")
public class DemoRestController {
//    http://localhost:8080/rest/getJson?id=1&name=hello
    @RequestMapping(value = "/getJson",produces = {"application/json;charset=UTF-8"})
    public DemoObj getJson(DemoObj obj){
        return new DemoObj(obj.getId()+1,obj.getName()+"kxc");
    }
    @RequestMapping(value = "/getXml",produces = {"application/xml;charset=UTF-8"})
    public DemoObj getXml(DemoObj obj){
        return new DemoObj(obj.getId()+1,obj.getName()+"yy");
    }
}
