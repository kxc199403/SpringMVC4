package com.kxc.advice;

import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by kk on 2017/8/7.
 */
@ControllerAdvice
public class ExceptionHandleAdvice {
    @ExceptionHandler(value = Exception.class)  //拦截所有的异常
    public ModelAndView exception(Exception exception, WebRequest request){
        ModelAndView modelAndView =new ModelAndView("error");
        modelAndView .addObject("errorMessage",exception.getMessage());
        return modelAndView;
    }
    @ModelAttribute
    public void addAttributes(Model model){
        model.addAttribute("msg","额外信息");
    }
    @InitBinder
    public void initBinder(WebDataBinder webDataBinder){
        webDataBinder.setDisallowedFields("id");
    }
}


























