package com.kxc.interceptor;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by kk on 2017/8/7.
 */
public class DemoInterceptor extends HandlerInterceptorAdapter {

    public boolean preHandle(HttpServletRequest request, HttpServletResponse response,Object handler) throws Exception{
        long startTime=System.currentTimeMillis();
        request.setAttribute("startTime",startTime);
        return true;
    }
    public void postHandle(HttpServletRequest request, HttpServletResponse response,
                           Object handler, ModelAndView modelAndView) throws Exception{
        long startTime =(Long) request.getAttribute("startTime");
        request.removeAttribute("startTime");
        Long endTime=System.currentTimeMillis();
        System.out.println("本次请求处理时间："+new Long(endTime-startTime)+"ms");
        request.setAttribute("handlingTime",endTime-startTime);
    }

}
