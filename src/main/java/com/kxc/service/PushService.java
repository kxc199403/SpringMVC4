package com.kxc.service;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.async.DeferredResult;

/**
 * Created by kk on 2017/8/7.
 */
@Service
public class PushService {
    private DeferredResult<String> deferredResult;

    public DeferredResult<String> getAsyncUpdate(){
        deferredResult=new DeferredResult<String>();
        return deferredResult;
    }
    @Scheduled(fixedDelay = 5000)  //每隔5秒自动触发一次
    public void refresh(){
        if(deferredResult!=null){
            deferredResult.setResult(Long.toString(System.currentTimeMillis()));
        }
    }
}
